package com.example.chetanps.taxiapp;

/**
 * Created by chetanps on 08-03-18.
 */

public class User {

    public String userEmail,userPassword,userName;

    public User(String userEmail, String userPassword,String userName){
        this.userEmail = userEmail;
        this.userPassword = userPassword;
        this.userName= userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
