package com.example.chetanps.taxiapp;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import java.util.List;

public class Register extends AppCompatActivity implements OnClickListener {

    EditText email, password, username;
    Button register;
    private FirebaseAuth auth;
    public String UserEmail, Userpassword, Username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);

        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        username = (EditText) findViewById(R.id.name);
        register = (Button) findViewById(R.id.Register);

        register.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        UserEmail = email.getText().toString().trim();
        Username = username.getText().toString().trim();
        Userpassword = password.getText().toString().trim();

        try{
            auth.createUserWithEmailAndPassword(UserEmail, Userpassword)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            Toast.makeText(Register.this, "createUserWithEmail:onComplete:" + task.isSuccessful(), Toast.LENGTH_SHORT).show();
                            //   progressBar.setVisibility(View.GONE);
                            // If sign in fails, display a message to the user. If sign in succeeds
                            // the auth state listener will be notified and logic to handle the
                            // signed in user can be handled in the listener.
                            if (!task.isSuccessful()) {
                                Toast.makeText(Register.this, "Authentication failed." + task.getException(),
                                        Toast.LENGTH_LONG).show();
                            } else {
                                startActivity(new Intent(Register.this, MainActivity.class));
                                finish();
                            }
                        }
                    });
        }catch (Exception ex){
            Toast.makeText(this, ex.toString(),Toast.LENGTH_LONG).show();
        }


    }
};

